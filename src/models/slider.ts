export class SliderClass {
  images!: string[]
  length!: number

  constructor(data:any) {
    this.images = data ? data : [] as string[]
    this.length = this.images.length
  }
}