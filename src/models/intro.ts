export class IntroClass {
  title!: string
  image!: string
  shortDescription!: string
  readingTime!: number
  viewsCount!: number

  constructor(data:any) {
    this.title = data.title ? data.title : ''
    this.image = data.image ? data.image : ''
    this.shortDescription = data.short_description ? data.short_description : ''
    this.readingTime = data.reading_time ? data.reading_time : ''
    this.viewsCount = data.views_count ? data.views_count : ''
  }
}