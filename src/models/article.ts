export class ArticleClass {
  title!: string
  image!: string
  link!: string

  constructor(data:any) {
    this.title = data.title ? data.title : ''
    this.image = data.image ? data.image : ''
    this.link = data.link ? data.link : ''
  }
}