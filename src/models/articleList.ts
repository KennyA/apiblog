import { ArticleClass } from './article'

export class ArticleListClass {
  title!: string
  articles!: ArticleClass[]

  constructor(data:any) {
    this.title = data.title ? data.title : ''
    this.articles = [] as ArticleClass[]
    data.articles.forEach((article:any) => {
      this.articles.push(new ArticleClass(article))
    })
  }
}