export class ImageClass {
  src!: string
  caption!: string

  constructor(data:any) {
    this.src = data.src ? data.src : ''
    this.caption = data.caption ? data.caption : ''
  }
}