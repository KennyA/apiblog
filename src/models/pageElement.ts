import { ArticleListClass } from './articleList'
import { IntroClass } from './intro'
import { ImageClass } from './image'
import { SliderClass } from './slider'
import { ArticleClass } from './article'

export class PageElementClass {
  data!: ArticleListClass | ImageClass | IntroClass | SliderClass | {}
  id!: string
  type!: string

  constructor(data:any) {
    this.id = data.id ? data.id : ''
    this.type = data.type ? data.type : ''
    
    if (this.type === 'article_list_block') {
      this.data = new ArticleListClass(data.data)
    } else if (this.type === 'article_intro_block') {
      this.data = new IntroClass(data.data)
    } else if (this.type === 'image_block') {
      this.data = new ImageClass(data.data)
    } else if (this.type === 'slider_block') {
      this.data = new SliderClass(data.data)
    } else {
      this.data = data.data
    }
  }
}