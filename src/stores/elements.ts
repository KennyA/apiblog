import { reactive, ref } from 'vue'
import type { Ref } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import { useHead } from '@vueuse/head'

import { PageElementClass } from '@/models/pageElement'

export const usePageElementsStore = defineStore('pageElements', () => {
  let type:Ref<string> = ref('')
  let elements:PageElementClass[] = reactive([] as PageElementClass[])

  const getPageData = async (url:string):Promise<void> => {
    await axios
      .get(`https://devtwit8.ru/api/v1/page/?path=${url}`)
      .catch((error) => console.log(error))
      .then((res:any) => {
        const data = res.data

        useHead({
          title: data.meta.title,
          meta: [
            {name: 'description', content: data.meta.description},
            {name: 'slug', content: data.meta.slug},
          ],
        })

        type.value = data.page_type
        data.body.forEach((el:any) => {
          elements.push(new PageElementClass(el))
        })
      })
  }

  return { type, elements, getPageData }
})
